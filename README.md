# dotfiles
My based void dotfiles. With wallpapers!
All for personal use, so don't expect this to be well organised.
It makes sense in MY head, ok?

My dwm config is at [this repo](https://github.com/Sanzarote/dwm)

## Screenshots

![TECHN0L0GY](scr/plex.png)

![Shining On](scr/sea.png)

![h4x0r5](scr/1337.png)

![leetle fetch](scr/plateau.png)
