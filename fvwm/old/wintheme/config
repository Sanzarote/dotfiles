SetEnv terminal st
SetEnv iconDir $HOME/.fvwm/icons

AddToFunc StartFunction
+ I Module FvwmCommands
+ I Module FvwmPager 0 0
+ I Exec xclock -digital -face "Terminus:antialias=false:Bold::size=15" -strftime "%I:%M:%S %p" -p 10 -geometry 132x38+10+162 -update 1 &
+ I Exec audacious

*FvwmPagerGeometry 184x125+10+10

HilightColor	#FFFFFF #000080

Style "*" Color #000000 #ffffff

# icons

## Set path to icons and pixmaps
ImagePath $[iconDir]:$[icondir];.png:+
####

### ## # 
#Desktops
####### ## #  # #   #

DesktopSize 3x3

EdgeScroll 0 0
EdgeResistance -1

#Colorset 17 fg black, bg #c0c0c0
Colorset 17 fg black, bg #808080

BorderStyle Simple
Style "*" BorderWidth 4, HandleWidth 4
Style "*" DumbPlacement

# Window decorations
Style *             Title, Handles, !MWMFunctions, NoDecorHint
Style "Fvwm*"		NoTitle, Sticky, WindowListSkip, NoHandles, BorderWidth 7
Style "xload"		NoTitle, Sticky, WindowListSkip, NoHandles, FPLenient, BorderWidth 7
Style "*lock"		NoTitle, Sticky, WindowListSkip, NoHandles, BorderWidth 7
Style "*audacious*"	NoTitle, Sticky, WindowListSkip, NoHandles, BorderWidth 0

# Generic menu style
MenuStyle black grey slategrey
MenuStyle "*" Font xft:Terminus:antialias=false:Regular::size=8
MenuStyle * VerticalItemSpacing 3 3

WindowFont              "xft:BmPlus IBM VGA 8x16:antialias=false:Normal::size=12"
IconFont              "xft:Terminus:antialias=false:Regular::size=9"
TitleStyle Centered Height 19 Vector 1 0x100@1 100x100@3 -- Flat

ButtonStyle 2	ActiveDown Pixmap icon_right2_press.png -- Flat 
ButtonStyle 2	ActiveUp Pixmap icon_right2.png -- Flat
ButtonStyle 2	InactiveDown Pixmap icon_right2_press.png -- Flat
ButtonStyle 2	InactiveUp Pixmap icon_right2.png -- Flat
ButtonStyle 4 ActiveDown Pixmap icon_right1_press.png -- Flat
ButtonStyle 4 ActiveUp Pixmap icon_right1.png -- Flat
ButtonStyle 4 InactiveDown Pixmap icon_right1_press.png
ButtonStyle 4 InactiveUp Pixmap icon_right1.png -- Flat
ButtonStyle 1	ActiveDown Pixmap icon_left_press.png -- Flat
ButtonStyle 1	ActiveUp Pixmap icon_left.png -- Flat
ButtonStyle 1	InactiveDown Pixmap icon_left_press.png -- Flat
ButtonStyle 1	InactiveUp Pixmap icon_left.png -- Flat

#Style "*" IconBox 5 5 1440 1080, IconGrid 1 1, IconFill Left Top
Style "*" IconBox 2 2 1920 1080, IconGrid 1 1, IconFill Left Bottom
Style * IconSize 32 32 128 128
Style * IconBackgroundColorset 17
Style * IconBackgroundRelief 0
Style * IconBackgroundPadding 0
Style * IconTitleRelief 0

Colorset 50 fg white, bg #008080
Colorset 51 fg white, bg #000080
Style * IconTitleColorset 50
Style * HilightIconTitleColorset 51


# icons do not update automatically on title change
Style * IconOverride
#original config line, I don't know what DEFAULT.PNG is
Style "*" Icon win95/DEFAULT.PNG
Style "st" Icon win95/term.png

# Keep the last focus while in root window
Style "*" SloppyFocus, MouseFocusClickRaises

# Click/release must occur in <n milliseconds to be a "click"
ClickTime 250

# Wireframe windows when moving
OpaqueMoveSize 0

# Menu for window operations
DestroyMenu MenuFvwmRoot
AddToMenu MenuFvwmRoot
+ "FVWM Menu" Module FvwmConsole
+ "" Nop
+ "Terminal" Exec exec st
+ "FvwmConsole" Module FvwmConsole 
+ "Refresh FVWM" Restart
+ "Quit FVWM" Quit

AddToMenu Window-Ops    "Window Ops "    Title
+                       "Move "          Move-or-Raise2
+                       "Resize "        Resize-or-Raise2
+                       "Raise "         Raise
+                       "Lower "         Lower
+                       "(De)Iconify "   Iconify
+                       "(Un)Stick "     Stick
+                       "(Un)Maximize "  Maximize
+                       "(No)Titlebar "  Pick (CirculateHit) ToggleTitleBar
+                       ""              Nop
+                       "Delete "        Delete
+                       "Close "         Close
+                       "Destroy "       Destroy
+                       ""              Nop
+                       "Refresh Screen " Refresh

# Menu for top left hand ("close") button on windows
AddToMenu Window-Ops2   "Raise/Lower "   RaiseLower
+                       "Iconify "       Iconify
+                       "(Un)Stick "     Stick
+                       ""              Nop
+                       "Delete "        Delete
+                       "Close "         Close
+                       "Destroy "       Destroy

## Start xClock on startup
# AddToFunc "InitFunction" I Exec xclock -digital -bg gray -fg black -face "Arial:antialias=false:Regular::size=15" -strftime "%I:%M:%S %p" -p 10 -geometry 110x38+5+183 -update 1 &

AddToFunc "InitFunction" I Exec hsetroot -tile ~/.fvwm/win31.png &

# windows

# This will automatically re-tile the icons in the icon box when
# a window is de-iconified
AddToFunc TileIcons
 + D Iconify off
 + D All (CurrentPage, Iconic) PlaceAgain Icon

AddToFunc Move-or-Raise		"I" Raise
+				"M" Move
+				"D" Lower

AddToFunc Move-or-Raise2        "M" Raise
+				"M" Move
+				"D" Lower

AddToFunc Resize-or-Raise       "I" Raise
+                               "M" Resize
+                               "D" Lower

AddToFunc Resize-or-Raise2      "M" Raise
+                               "M" Resize
+                               "D" Lower

AddToFunc PrintFunction         "I" Raise
+                               "I" Exec xdpr -id $w

AddToFunc Iconify-and-Raise     "I" Iconify
+                               "I" Raise

AddToFunc Maximize-or-Move
 + D Maximize
 + I Raise
 + C Focus
 + M Move

# mouse

Mouse 1		R   	A       Nop

Mouse 1		R    	A 	Menu MenuFvwmRoot
Mouse 3		R    	A       Menu Window-Ops Nop
Mouse 2		R    	A      	WindowList

# Left button is the Window-Ops2 menu, right is iconify, rightmost is maximize
#     Button	Context Modifi 	Function
Mouse 0		1       A       Menu Window-Ops2 Close
Mouse 0		2    	A     	Maximize
Mouse 0		4    	A     	Iconify

# Button 1 in Frame or Sides is a resize-or-raise, in Icons is a tiling deiconify
# and on Top does a move-or-raise
#     Button	Context Modifi 	Function
Mouse 1 	FS      A       Resize-or-Raise
Mouse 1		I       A       TileIcons
Mouse 1		T       A       Maximize-or-Move

# Button 2 in an Icon is de-iconify, in Corners/Sides/Titlebar gets Ops Menu
#     Button	Context Modifi 	Function
Mouse 2 	I       A       Iconify
Mouse 2 	FST     A       Menu Window-Ops2 Nop

# Button 3 does a raise-lower on anything
Mouse 3 	TSIF    A       RaiseLower

# keyboard

## Paging: Windows key + arrows moves to the next desktop
## in the desired direction
Key Left	A	4	Scroll -100 0
Key Right	A	4	Scroll +100  +0
Key Up		A	4	Scroll +0 -100
Key Down	A	4	Scroll +0 +100
Key Return	A	4	Exec exec st
Key D		A	4	Exec exec dmenu_run
Key 1		A	4	GotoDesk 0
Key 2		A	4	GotoDesk 1
#Key 3		A	4	GotoDesk 2
#Key 4		A	4	GotoDesk 3
#Key 5		A	4	GotoDesk 4
#Key 6		A	4	GotoDesk 5
#Key 7		A	4	GotoDesk 6
#Key 8		A	4	GotoDesk 7
#Key 9		A	4	GotoDesk 8
## Fast Alt-Tab
Key Tab A M WindowList Root c c CurrentDesk, NoGeometry, CurrentAtEnd, IconifiedAtEnd
